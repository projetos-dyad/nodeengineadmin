# EngineAdmin #

Aplicativo para administração de caches locais do aplicativo iEngine.exe.


### A quem se destina? ###

A desenvolvedores de software que utilizam o servidor de aplicação iEngine da empresa Bematech.

### Instalação ###

* Usando o git:
    1. $ git clone REPOSITORIO
    2. $ cd engineadmin && npm install
    3. $ node app.js
* Características:
    1. Em cada cache instalado pelo aplicativo será criado um arquivo "base.cfg", que é um arquivo do tipo INI contendo as informações básicas sobre aquele cache, como nome, grupo e endereço da base servidora. O grupo é utilizado (claro) para agrupar engines relacionados no menu da aplicação.
    2. A aplicação organiza os caches em disco com base nos grupos. Por exemplo:
        - /home/ricardo/Bases/GRUPO1/CACHE1
        - /home/ricardo/Bases/GRUPO1/CACHE2
        - /home/ricardo/Bases/GRUPO2/CACHE1
    3. Por causa dos dois itens anteriores, se você já possui todos os caches juntos numa mesma pasta, não é indicado simplesmente apontar a aplicação para essa pasta. Nesse caso, o mais indicado é apontar a aplicação para uma pasta nova e ir instalando os caches pelo próprio aplicativo, para que ele mesmo crie as pastas na hierarquia descrita no item 2 e também o arquivo descrito no item 1;
* Configuração: Para informar a pasta principal, onde a aplicação irá instalar os caches, é necessário informar o parâmetro --basedir ou definir a variável de ambiente ENGINE_PATH:
    1. node app.js --basedir /home/ricardo/Bases
    2. ENGINE_PATH=/home/ricardo/Bases node app.js

### Screenshot ###

-![Captura_de_tela-Cache Admin - Mozilla Firefox-2.png](https://bitbucket.org/repo/poegzp/images/1763655440-Captura_de_tela-Cache%20Admin%20-%20Mozilla%20Firefox-2.png)


