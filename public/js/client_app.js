var lista       = null;
var grupos      = null;
var grupo_atual = null;
var tplTitulo   = null;
var tplTabela   = null;
var tplForm     = null;
var tplAlerta   = null;
var tplMenu     = null;
var tplProgress = null;
var socket      = null;

/**
 * Retona os dados da base pelo id
 *
 * @param id Nome da Bases
 * @returns {*} Objeto com os dados da base
 */
function findById(id) {
    for (var i = 0; i < lista.length; i++) {
        if ( lista[i].id == id ) {
            return lista[i];
        }
    }
    return null;
}

function carrega_templates(){
    tplMenu   = Handlebars.compile($("#template-menu").html());
    tplTitulo = Handlebars.compile($("#template-titulo").html());
    tplTabela = Handlebars.compile($("#template-tabela").html());
    tplAlerta = Handlebars.compile($("#template-alerta").html());
    tplForm   = Handlebars.compile($("#formulario-base").html());
    tplProgress = Handlebars.compile($("#template-progress").html());
}

function func_ordena(baseA, baseB){

    //return (baseA.grupo + baseA.nome) > (baseB.grupo + baseB.nome);
    return [baseA.grupo, baseA.nome] > [baseB.grupo, baseB.nome] ? 1 : -1;
}

function inicializa_tela( refresh ){

   $.ajax({
        url: ( refresh ? '/refresh' : '/caches' ),
        dataType:"json"
    })
    .fail(function(req){
        console.log('Erro inicializar a tela:'+ req.responseText);
        showMessage('danger', 'Erro inicializar a tela:'+ req.responseText );
    })
    .done(function(caches){
        console.log('Recebeu a lista de caches. Montando a tela...');
        lista = caches;

        lista.sort( func_ordena );

        console.log('Populando o menu...');

        popula_menu();

        if ( grupo_atual ) {
            console.log('Filtrando bases do grupo '+ grupo_atual );
            filtraBases( grupo_atual );
        } else {
            console.log('Sem grupo para filtrar...');
        }
    });
}

function popula_menu(){
    if ( grupos ) {
        grupos.length = 0;
    } else {
        grupos = [];
    }

    for (var i = 0; i < lista.length; i++) {
        if ( grupos.indexOf(lista[i].grupo) < 0 ) {
            grupos.push( lista[i].grupo );
        }
    }
    $("#area-lateral").html( tplMenu( {grupos : grupos} ) );
}

/**
 * Função chamada quando o usuário seleciona um grupo no menu lateral.
 * Ela recebe o grupo passado por parâmetro e filtra as bases para exibição na
 * tabela principal.
 **/
function filtraBases( grupo ){
    grupo_atual = grupo;
    console.log('Filtrando bases do grupo: '+ grupo);
    var bases = [];
    for (var i = 0; i < lista.length; i++) {
        if ( lista[i].grupo == grupo) {
            bases.push( lista[i] );
        }
    }
    bases.sort( func_ordena );

    $('#area-titulo').html( tplTitulo( {titulo: 'Bases '+ grupo} ) );
    $("#area-tabela").html( tplTabela( {bases : bases} ) );
}

function abrePasta( pasta ){
    $.post( "/folder", { folder: pasta } )
    .fail(function(req){
        console.log('Erro ao abrir a pasta:'+ req.responseText);
        showMessage('danger', 'Erro ao abrir a pasta:'+ req.responseText );
    });
}

/**
 * Abre o formulário de parametrização da base selecionada
 *
 * @param baseSelecionada
 */
function cfgBase( id ) {

    var baseSelecionada = null;

    if ( id ) {
        baseSelecionada = findById( id );
    };

    var cfg = bootbox.dialog({
        title       : 'Criação de arquivo de configuração',
        closeButton : false,
        message     : tplForm,
        show        : false,
        buttons     : {
            cancel: {
                label: 'Cancelar',
                className: "btn-danger"
            },
            sucess: {
                label: 'Confirmar',
                callback: function btn_suc_callback() {

                    var dados = $('form.configuracao').serialize();
                    console.log("Atualizando com dados:"+ dados);
                    // Atualização de um cache existente
                    if ( id ) {

                        $.ajax('/caches/'+ id, {
                            type : 'PUT',
                            data : dados
                        })
                        .fail(function caches_put_fail( response ){
                            var decodedError = JSON.parse(response.responseText);
                            bootbox.alert( decodedError.message );
                        })
                        .done(function caches_put_done( data ){
                            recarregar();
                        });
                    // Criação de um novo cache
                    } else {
                        $.post('/caches', dados)
                        .fail(function caches_post_fail( response ){
                            var decodedError = JSON.parse(response.responseText);
                            bootbox.alert( decodedError.message );
                        })
                        .done(function caches_post_done( data ){
                            recarregar();
                        });
                    }
                }
            }
        }
    });

    cfg.on("shown.bs.modal", function() {

        if ( ! baseSelecionada ) {
            return;
        }

        // console.log("Parametros adicionais:"+ baseSelecionada.params);

        var formulario = $('form.configuracao');

        formulario.find( 'input[name="nome"]').val( baseSelecionada.nome );
        formulario.find( 'input[name="grupo"]').val( baseSelecionada.grupo );
        formulario.find( 'input[name="url"]').val( baseSelecionada.url );
        formulario.find( 'input[name="params"]').val( baseSelecionada.params );
        formulario.find( 'input[name="pasta"]').val( baseSelecionada.pasta );
        if ( !baseSelecionada.arch || baseSelecionada.arch === '64' ) {
            formulario.find( 'input[name="arch"]').prop('checked', false);
        } else {
            formulario.find( 'input[name="arch"]').prop('checked', true);
        }
    });

    cfg.modal();
}

function startaBase( id ){

    var baseSelecionada = findById(id);

    if ( ! baseSelecionada || baseSelecionada.nome === undefined || baseSelecionada.nome === null ) {

        return bootbox.dialog({
            title           : 'Erro ao carregar o arquivo de configuração.',
            message         : 'O arquivo de configuração não foi encontrado para esta base, deseja criá-lo ?',
            closeButton     : false,
            onEscape        : false,
            buttons         : {
                cancel: {
                    label       : 'Cancelar',
                    className   : "btn-danger"
                },
                success     : {
                    label       : "Sim, ir para criação das configurações",
                    callback: function ( dialog ) {
                        return cfgBase( baseSelecionada );
                    }
                }
            }
        });

    }

    $.ajax("/start/"+ id)
        .fail(function(req){
            console.log('Erro ao subir a base:'+ req.responseText);
            showMessage('danger', 'Erro ao subir a base:'+ req.responseText );
        });
}

function recarregar(){
    lista       = null;
    grupos      = null;
    //grupo_atual = null;

    inicializa_tela( true );
}

function apagarCache( id ){
    if ( ! confirm('Tem certeza???') ) {
        return;
    }

    $.ajax("/caches/"+ id +'/all', {type:'DELETE'})

    .fail(function(req){
        showMessage('danger', 'Erro ao apagar o cache:'+ req.responseText );
    })

    .done( function(){
        recarregar();
        showMessage('info', 'Cache excluido com sucesso.');
    });
}

function descartaCache( id ){
    if ( ! confirm('Tem certeza???') ) {
        return;
    }

    $.ajax("/caches/"+ id +'/cache', {type:'DELETE'})

    .fail(function(req){
        showMessage('danger', 'Erro ao descartar o cache:'+ req.responseText );
    })

    .done(function(){
        showMessage('info', 'Cache descartado com sucesso.');
    });
}

function showMessage(tipo, msg){

    $('#area-tabela').prepend( tplAlerta( { tipo: tipo, mensagem: msg } ) );
}

function descartaChaves( nome ){}

function conecta_websocket(){
    socket = io.connect('http://127.0.0.1:3000', {'force new connection': true});
    socket.on('connect', function(){
        console.log('Conectou ao server!');
    });
    socket.on('disconnect', function(){
        console.log('Desconectou do server!');
    });
    socket.on('error', function(){
        console.log('Erro!');
    });
    socket.on('downloading', function(data){
        var perc = JSON.parse(data).percent;
        if (perc < 100) {
            $('#area-notificacao').html(tplProgress({percent: perc}));
        } else {
            $('#area-notificacao').html('');
        }
    });
}

// Registra o callback de tratamento para quando o HTML da página terminar de carregar
$(document).ready(function(){
    carrega_templates();
    inicializa_tela();
    conecta_websocket();
});