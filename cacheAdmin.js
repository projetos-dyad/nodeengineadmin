var glob = require('glob');
var path = require('path');
var http = require('http');
var ini  = require('ini');
var fs   = require('fs');
var os   = require('os');
var spawn = require('child_process').spawn;
var shortid = require('shortid');
// var io = require('socket.io');

process.env.DISPLAY = ":0";

var rmdir = function(dir) {
    var list = fs.readdirSync(dir);
    for(var i = 0; i < list.length; i++) {
        var filename = path.join(dir, list[i]);
        var stat = fs.statSync(filename);
        if(filename == "." || filename == "..") {
            // pass these files
        } else if(stat.isDirectory()) {
            rmdir(filename);
        } else {
            fs.unlinkSync(filename);
        }
    }
    fs.rmdirSync(dir);
};

module.exports = {

    /**
     * Função responsável por carregar as informações dos caches existentes em disco
     **/
    load : function cacheAdmin_load( basedir ){
        console.log('Carregando os caches a partir da pasta: '+ basedir );
        this.basedir = basedir;
        this.bases = [];
        var arquivos = glob.sync( path.join(basedir,'**/i[e,E]ngine.exe'));
        for (var i = arquivos.length - 1; i >= 0; i--) {
            if ( (arquivos[i]).indexOf("/dist") >= 0 ) {
                continue;
            }
            this.bases.push( this.retornaBasePorReferenciaAoExecutavel(arquivos[i]) );
        }
        var arquivos64 = glob.sync( path.join(basedir,'**/Engine64.exe'));
        for (var i = arquivos64.length - 1; i >= 0; i--) {
            if ( (arquivos64[i]).indexOf("/dist") >= 0 ) {
                continue;
            }
            this.bases.push( this.retornaBasePorReferenciaAoExecutavel(arquivos64[i]) );
        }
        console.log('Carregadas '+ this.bases.length +' base(s)')
    },

    retornaBasePorReferenciaAoExecutavel : function retornaBase(iengine_exe){
        var pasta   = path.dirname( iengine_exe );
        var grupo   = '** DESCONHECIDAS **';
        var nome    = null;
        var url     = null;
        var params  = null;

        var config      = path.join(pasta,'base.cfg');
        var objconfig   = null;

        if ( fs.existsSync( config ) ) {
            objconfig   = ini.parse(fs.readFileSync( config, 'utf-8'));
            grupo       = objconfig.grupo;
            nome        = objconfig.nome;
            url         = objconfig.url;
            params      = objconfig.params;
        }
        return {
            id      : shortid.generate(),
            pasta   : pasta,
            grupo   : grupo,
            nome    : nome,
            url     : url,
            params  : params
        }
    },

    list : function cacheAdmin_list(){

        return this.bases;
    },

    get : function cacheAdmin_get( id ){
        for (var i = 0; i < this.bases.length; i++) {
            if ( this.bases[i].id == id ) {
                return this.bases[i];
            }
        }
        return null;
    },

    createCache : function cacheAdmin_create( novo, canal ){

        console.log('Dados do novo cache: %j', novo );
        var pasta = this.createDir( novo );
        var config = this.createConfig( novo, pasta );

        this.downloadEngine( config, canal );
    },

    updateCache : function cacheAdmin_update( params ){
        var config_file = path.join(params.pasta,'base.cfg');

        var obj = {
            grupo :   params.grupo,
            nome :    params.nome,
            url :     params.url,
            params:   params.params,
            pasta:    params.pasta
        }

        if ( params.arch == 'on' ) {
            obj.arch = 64;
        } else {
            obj.arch = 32;
        }
        console.log('Dados da base para atualizar: %j', obj);

        fs.writeFileSync(
            config_file,
            ini.stringify(obj)
        );
    },

    openFolder : function cacheAdmin_openFolder( params ){
        var comando = null;
        var parametros = null;
        var sistema = os.type();

        if ( sistema == 'Linux' ) {
            comando = '/usr/bin/gio';
            parametros = [ 'open', params.folder ];
        } else if ( sistema == 'Darwin'){
            comando = 'open';
            parametros = [ params.folder ];
        } else {
            comando = 'explorer.exe';
            parametros = [ params.folder.replace ( /\//g, '\\') ];
        }
        try {
            var out = fs.openSync(path.join(params.folder,'out.log'), 'a');
            var err = fs.openSync(path.join(params.folder,'out.log'), 'a');
            var child = spawn( comando, parametros, {
                detached: true,
                stdio: [ 'ignore', out, err ]
            });
            child.unref();
        } catch(e){
            console.log("Erro: "+ e);
        }
    },

    createDir : function cacheAdmin_createDir( novo ){
        if ( ! fs.existsSync( this.basedir )) {
            throw "Diretorio não encontrado: "+ this.basedir;
        }
        console.log('Tentando criar o diretorio da nova base...')
        var dir_grupo = path.join(this.basedir, novo.grupo);
        var dir_base = path.join(dir_grupo, novo.nome);

        if ( ! fs.existsSync( dir_grupo )) {
            console.log('Criando a pasta do grupo...')
            fs.mkdirSync( dir_grupo );
        }
        if ( ! fs.existsSync( dir_base )) {
            console.log('Criando a pasta da base...')
            fs.mkdirSync( dir_base );
        }
        return dir_base;
    },

    createConfig : function cacheAdmin_createConfig( novo, pasta ){
        console.log('Ok, agora vou tentar criar o arquivo de configuração da nova base...')
        var arquivo_configuracao = path.join(pasta,'base.cfg');
        if ( fs.existsSync( arquivo_configuracao )) {
            throw "Arquivo de configuração já existe!";
        }
        var obj = {
            grupo :   novo.grupo,
            nome :    novo.nome,
            url :     novo.url,
            params:   novo.params,
            pasta:    pasta
        }
        if ( novo.arch == 'on' ) {
            obj.arch = 64;
        } else {
            obj.arch = 32;
        }

        fs.writeFileSync(
            arquivo_configuracao,
            ini.stringify(obj)
        );
        return obj;
    },

    downloadEngine : function cacheAdmin_downloadEngine(config){
        var iengine_exe = path.join(config.pasta, 'iEngine.exe');

        if ( fs.existsSync( iengine_exe ) ) {
            console.log( 'O arquivo iEngine.exe já existe na base ' + config.pasta );
            return 'O arquivo iEngine.exe já existe na base ' + config.pasta;
        }

        var self = this;
        var file = fs.createWriteStream( iengine_exe );

        http.get( 'http://'+ config.url +"/iEngine.exe", function( response ) {
            var len = parseInt(response.headers['content-length'], 10);
            var cur = 0;

            response.pipe(file);

            response.on('data', function(chunk){
                cur += chunk.length;
                var perc = (100.0 * cur / len).toFixed();
                if (self.canal && perc <= 100) {
                    self.canal.sockets.emit('downloading', JSON.stringify({
                        size: len,
                        percent: perc
                    }));
                }
            });
        });
    },

    startCache : function cacheAdmin_startCache( config ){
        console.log('Pasta:'+ config.pasta);

        var iengine_exe = path.join(config.pasta, 'iEngine.exe');
        if ( ! fs.existsSync( iengine_exe )) {

            iengine_exe = path.join(config.pasta, 'iengine.exe');
            if ( ! fs.existsSync( iengine_exe )) {

                iengine_exe = path.join(config.pasta, 'Engine64.exe');
                if ( ! fs.existsSync( iengine_exe )) {
                    throw 'Arquivo nao encontrado:'+ iengine_exe;
                }
            }
        }
        var sistema = os.type();
        var comando = null;
        var parametros = [];
        if ( sistema == 'Linux' || sistema == 'Darwin' ) {
            comando = '/usr/bin/wine';
            parametros.push(iengine_exe);
            parametros.push(config.url);
            parametros.push(config.nome);
            if ( config.params ) {
                parametros.push(config.params);
            }
        } else {
            comando = iengine_exe;
            parametros.push(config.url);
            parametros.push(config.nome);
            if ( config.params ) {
                parametros.push(config.params);
            }
        }
        var out = fs.openSync(path.join(config.pasta,'out.log'), 'a');
        var err = fs.openSync(path.join(config.pasta,'out.log'), 'a');
        console.log( comando +' '+ parametros );
        var child = spawn( comando, parametros, {
            detached: true,
            stdio: [ 'ignore', out, err ]
        });
        child.unref();
    },

    deleteCache: function cacheAdmin_deleteCache( config, tipo ){
        console.log('Dados da base: %j', config);
        if ( tipo == 'all' ) {
            console.log('Apagando a base ...');
            rmdir( config.pasta );
        } else {
            console.log('Apagando o cache ...')
            rmdir(path.join(config.pasta, 'dbCache'));
            rmdir(path.join(config.pasta, 'dbCacheBackup'));
            rmdir(path.join(config.pasta, 'dbCacheProvider'));
        }
    },

    deleteKeyCache : function cacheAdmin_deleteKeyCache(){}
}
