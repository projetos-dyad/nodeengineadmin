// TODOs
// - Incluir a opção de 2 URLs

var cacheAdmin  = require('./cacheAdmin.js');
var fs          = require('fs');
var express     = require('express');
var io          = require('socket.io');
var path        = require('path');
var bodyParser  = require('body-parser');
var favicon     = require('serve-favicon');

var basedir = process.env.ENGINE_PATH || null;
if ( process.argv[2] == '--basedir' && process.argv[3] ) {
    basedir = process.argv[3];
}
if (basedir == null) {
    console.log(
        "Informe a pasta base dos caches locais passando o parâmetro '--basedir' "+
        "ou a definindo a variável de ambiente 'ENGINE_PATH'.");
    process.exit(1);
}

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: 'application/json' }));
app.use( favicon( path.join( __dirname, 'public', 'img', 'e.ico' ) ) );

/*
================================================================================
API
================================================================================
GET     /caches             Retorna uma lista das bases encontradas no sistema  [OK]
GET     /caches/<nome>      Retorna os dados da base informada                  [OK]
POST    /caches             Cria um novo cache local                            [OK]
PUT     /caches/<nome>      Altera os dados de uma base (quase sempre URL)
DEL     /caches/<nome>      Por padrão apaga a basa. Permite passar o parametro "type" para informar se é a base, o cache ou as chaves
GET     /start/<nome>       Sobe o cache da base informada                      [OK]
*/

function index(req, res){
    //
    res.render('index',{title:"oi"});
}

function get_all_caches(req, res){
    //
    res.send( JSON.stringify(cacheAdmin.list(), null, 4) );
}

function new_cache(req, res){
    try {
        cacheAdmin.createCache(req.body);
    } catch(e){
        return res.status( 500 ).send( {
            success : false,
            message : 'Ocorreu uma falha na criação da base <b> ' + req.body.nome + '</b><br />' + e.toString()
        } );
    }
    res.sendStatus(200);
}

function start_cache(req, res){
    console.log('Subindo o cache '+ req.params.id);
    try{
        var cache = cacheAdmin.get( req.params.id );
        if ( ! cache ) {
            res.sendStatus(404);
            return;
        }
        console.log('Iniciando a base: '+ cache.nome )
        cacheAdmin.startCache(cache);
        res.sendStatus(200);
    } catch(e){
        console.log(e);
        res.status(500).send(e.toString());
    }
}

function delete_cache(req, res){
    console.log('Remover cache. Tipo: '+ req.params.tipo +'; Base:'+ req.params.id )
    try{
        var cache = cacheAdmin.get( req.params.id );
        if ( ! cache ) {
            res.sendStatus( 404 );
            return;
        }
        cacheAdmin.deleteCache( cache, req.params.tipo );
        res.sendStatus( 200 );
    } catch(e){
        console.log( e );
        res.status( 500 ).send( e.toString() );
    }
}

function delete_keycache(req, res){
    try{
        var cache = cacheAdmin.get( req.params.nome );
        if ( ! cache ) {
            res.sendStatus(404);
        }
        cacheAdmin.deleteKeyCache(cache);
        res.sendStatus(200);
    } catch(e){
        console.log(e);
        res.sendStatus(500);
    }
}

function open_folder(req, res){
    try {
        cacheAdmin.openFolder(req.body);
        res.sendStatus(200);
    } catch(e){
        res.status(500).send(e.toString());
    }
}

function get_refresh(req, res){
    console.log('Iniciando recarga da lista de bases...')
    cacheAdmin.load( basedir );
    console.log('Recarga realizada, retornando a lista...');
    res.send( cacheAdmin.list() );
}

function update_cache_info(req, res){
    try {
        cacheAdmin.updateCache(req.body);
    } catch(e){
        return res.status( 500 ).send( {
            success : false,
            message : 'Ocorreu uma falha na atualização da base <b> ' + req.body.nome + '</b><br />' + e.toString()
        } );
    }
    res.sendStatus(200);
}

app.get(    '/',                    index );
app.get(    '/caches',              get_all_caches );
app.post(   '/caches',              new_cache );
app.put(    '/caches/:id',          update_cache_info );
app.delete( '/caches/:id/:tipo',    delete_cache );
app.get(    '/start/:id',           start_cache );
app.post(   '/folder',              open_folder );
app.get(    '/refresh',             get_refresh );
app.get(    '/socket.io', function(req, res){
    res.sendStatus(200);
});
app.post(    '/socket.io', function(req, res){
    res.sendStatus(200);
});

var ws = io(app.listen(3000));

cacheAdmin.canal = ws;
cacheAdmin.load( basedir );

console.log('A mágica acontece na porta 3000');

